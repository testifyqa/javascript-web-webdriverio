import { Given, When, Then } from 'cucumber';
import redditPage from '../page-objects/reddit.page';

Then(/^I see the Reddit homepage$/, () => {
  const red = new redditPage();
  expect(red.promotedLink.isVisible());
  expect(red.title).to.contain('reddit:');
  expect(red.url).to.contain('https://www.reddit.com');
  expect(red.source).to.contain('Reddit');
});
